import Blog from "./blog";
import Features from "./features";
import Footer from "./footer";
import Header from "./header";
import Possibility from "./possibility";
import WhatChatGPT3 from "./whatGPT3";

export { Blog, Features, Footer, Header, Possibility, WhatChatGPT3 }